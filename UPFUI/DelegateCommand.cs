﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace UPFUI
{
	public class DelegateCommand : ICommand
	{
		public event EventHandler CanExecuteChanged { add { } remove { } }
		public readonly Action<object> action;
		public DelegateCommand(Action<object> action)
		{
			this.action = action;
		}

		public bool CanExecute(object paramater)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			action?.Invoke(parameter);
		}
	}
}
