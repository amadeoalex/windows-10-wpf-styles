﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using Windows.UI.ViewManagement;

namespace UPFUI
{
	public static class ThemeResources
	{
		public enum Themes
		{
			Light = 0,
			Dark = 1,
			System = 16
		}

		public static Themes ThemeSetting { get; set; } = Themes.Light;
		public static Themes SystemTheme { get; } = GetSystemTheme();
		public static Themes Theme { get => ThemeSetting == Themes.System ? SystemTheme : ThemeSetting; }
		//TODO: remove in preparation for white/light windows theme
		public static bool IsDarkTheme { get => Theme == Themes.Dark; }

/*		#region Brushes
		public static Brush ForegroundBrush { get; }
		public static Brush BackgroundBrush { get; }

		public static Brush AccentBrush { get; }
		public static Brush AccentBrushDark1 { get; }
		public static Brush AccentBrushDark2 { get; }
		public static Brush AccentBrushDark3 { get; }
		public static Brush AccentBrushLight1 { get; }
		public static Brush AccentBrushLight2 { get; }
		public static Brush AccentBrushLight3 { get; }

		public static Brush StatusBarBrush { get; }
		#endregion*/

		static ThemeResources()
		{ 
/*			ForegroundBrush = new SolidColorBrush(ThemeColors.ForegroundColor);
			BackgroundBrush = new SolidColorBrush(ThemeColors.BackgroundColor);

			AccentBrush = new SolidColorBrush(ThemeColors.AccentColor);
			AccentBrushDark1 = new SolidColorBrush(ThemeColors.AccentColorDark1);
			AccentBrushDark2 = new SolidColorBrush(ThemeColors.AccentColorDark2);
			AccentBrushDark3 = new SolidColorBrush(ThemeColors.AccentColorDark3);
			AccentBrushLight1 = new SolidColorBrush(ThemeColors.AccentColorLight1);
			AccentBrushLight2 = new SolidColorBrush(ThemeColors.AccentColorLight2);
			AccentBrushLight3 = new SolidColorBrush(ThemeColors.AccentColorLight3);

			StatusBarBrush = new SolidColorBrush(ThemeColors.StatusBarColor);*/
		}

		private static Themes GetSystemTheme()
		{
			const string registryKey = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize";
			const string registryValue = "AppsUseLightTheme";
			int value = (int)Registry.GetValue(registryKey, registryValue, 0);
			return value == 1 ? Themes.Light : Themes.Dark;
		}
	}
}
