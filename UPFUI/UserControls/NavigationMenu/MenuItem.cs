﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace UPFUI.UserControls.NavigationMenu
{
	public sealed class MenuItem : IMenuItem
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public string Id { get; set; }
		public string IconChar { get; set; }
		public string Title { get; set; }
	}
}
