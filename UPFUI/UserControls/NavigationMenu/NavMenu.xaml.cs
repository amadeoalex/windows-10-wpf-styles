﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;

namespace UPFUI.UserControls.NavigationMenu
{
	/// <summary>
	/// Interaction logic for NavMenu.xaml
	/// </summary>
	public partial class NavMenu : UserControl, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(nameof(Title), typeof(string), typeof(NavMenu));
		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public static readonly DependencyProperty MenuItemsSourceProperty = DependencyProperty.Register(nameof(MenuItemsSource), typeof(IEnumerable<IMenuItem>), typeof(NavMenu));
		public IEnumerable<IMenuItem> MenuItemsSource
		{
			get { return (IEnumerable<IMenuItem>)GetValue(MenuItemsSourceProperty); }
			set { SetValue(MenuItemsSourceProperty, value); }
		}

		public static readonly DependencyProperty FooterItemsSourceProperty = DependencyProperty.Register(nameof(FooterItemsSource), typeof(IEnumerable<IMenuItem>), typeof(NavMenu));
		public IEnumerable<IMenuItem> FooterItemsSource
		{
			get { return (IEnumerable<IMenuItem>)GetValue(FooterItemsSourceProperty); }
			set { SetValue(FooterItemsSourceProperty, value); }
		}

		public static readonly DependencyProperty OnItemClickCommandProperty = DependencyProperty.Register(nameof(OnItemClickCommand), typeof(ICommand), typeof(NavMenu));
		public ICommand OnItemClickCommand
		{
			get { return (ICommand)GetValue(OnItemClickCommandProperty); }
			set { SetValue(OnItemClickCommandProperty, value); }
		}

		public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(nameof(SelectedItem), typeof(IMenuItem), typeof(NavMenu), new FrameworkPropertyMetadata(SelectedItemChanged) { BindsTwoWayByDefault = true });

		public IMenuItem SelectedItem
		{
			get { return (IMenuItem)GetValue(SelectedItemProperty); }
			set { SetValue(SelectedItemProperty, value); }
		}

		public ICommand ItemClickedCommand { get; }
		public NavMenu()
		{
			InitializeComponent();
			ItemClickedCommand = new DelegateCommand((itemObject) =>
			{
				MenuItem menuItem = itemObject as MenuItem;
				SelectedItem = menuItem;

				if (OnItemClickCommand != null && OnItemClickCommand.CanExecute(menuItem))
					OnItemClickCommand.Execute(menuItem);
			});
		}

		private static void SelectedItemChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
		{
			NavMenu navMenu = dependencyObject as NavMenu;
			MenuItem menuItem = (MenuItem)args.NewValue;

			foreach (var item in navMenu.menuItemsControl.Items)
			{
				ContentPresenter contentPresenter = (ContentPresenter)navMenu.menuItemsControl.ItemContainerGenerator.ContainerFromItem(item);
				contentPresenter.ApplyTemplate();
				NavItem navItem = (NavItem)contentPresenter.ContentTemplate.FindName("itemControl", contentPresenter);
				navItem.Selected = false;
			}
			foreach (var item in navMenu.footerItemsControl.Items)
			{
				ContentPresenter contentPresenter = (ContentPresenter)navMenu.footerItemsControl.ItemContainerGenerator.ContainerFromItem(item);
				contentPresenter.ApplyTemplate();
				NavItem navItem = (NavItem)contentPresenter.ContentTemplate.FindName("itemControl", contentPresenter);
				navItem.Selected = false;
			}

			ContentPresenter presenter;
			if (navMenu.MenuItemsSource.Contains(menuItem))
				presenter = (ContentPresenter)navMenu.menuItemsControl.ItemContainerGenerator.ContainerFromItem(menuItem);
			else
				presenter = (ContentPresenter)navMenu.footerItemsControl.ItemContainerGenerator.ContainerFromItem(menuItem);

			presenter.ApplyTemplate();
			((NavItem)presenter.ContentTemplate.FindName("itemControl", presenter)).Selected = true;
		}
	}
}
