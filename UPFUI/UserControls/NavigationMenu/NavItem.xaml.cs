﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UPFUI.UserControls.NavigationMenu
{
	/// <summary>
	/// Interaction logic for NavItem.xaml
	/// </summary>
	public partial class NavItem : UserControl, INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public static readonly DependencyProperty IconCharProperty = DependencyProperty.Register(nameof(IconChar), typeof(string), typeof(NavItem), new FrameworkPropertyMetadata(null, IconChanged));
		public string IconChar
		{
			get { return (string)GetValue(IconCharProperty); }
			set { SetValue(IconCharProperty, value); }
		}

		public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(nameof(Title), typeof(string), typeof(NavItem));
		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public static readonly DependencyProperty SelectableProperty = DependencyProperty.Register(nameof(Selectable), typeof(bool), typeof(NavItem), new FrameworkPropertyMetadata(true, SelectableChanged));
		public bool Selectable
		{
			get { return (bool)GetValue(SelectableProperty); }
			set { SetValue(SelectableProperty, value); }
		}

		public static readonly DependencyProperty SelectedProperty = DependencyProperty.Register(nameof(Selected), typeof(bool), typeof(NavItem), new FrameworkPropertyMetadata(false, SelectedChanged));
		public bool Selected
		{
			get { return (bool)GetValue(SelectedProperty); }
			set { SetValue(SelectedProperty, value); }
		}

		public static readonly DependencyProperty OnClickCommandProperty = DependencyProperty.Register(nameof(OnClickCommand), typeof(ICommand), typeof(NavItem));
		public ICommand OnClickCommand
		{
			get { return (ICommand)GetValue(OnClickCommandProperty); }
			set { SetValue(OnClickCommandProperty, value); }
		}

		public Color HoverBackgroundColor { get; } = ThemeColors.BaseLow;

		public Color HoverBrushColor { get; }

		public NavItem()
		{
			HoverBrushColor = GetHoverBrushColor();
			InitializeComponent();
		}

		private Color GetHoverBrushColor()
		{
			byte alpha = (byte)(HoverBackgroundColor.A - 0x22);
			return Color.FromArgb(alpha, HoverBackgroundColor.R, HoverBackgroundColor.G, HoverBackgroundColor.B);
		}

		private static void IconChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
		{
			NavItem item = dependencyObject as NavItem;
			string ichonChar = (string)args.NewValue;
			double width = string.IsNullOrWhiteSpace(ichonChar) ? 12 : item.iconGrid.MaxWidth;

			item.iconGrid.Width = width;
		}

		private static void SelectedChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
		{
			NavItem item = dependencyObject as NavItem;
			bool selected = (bool)args.NewValue;

			item.indicator.Visibility = selected ? Visibility.Visible : Visibility.Hidden;
		}

		private static void SelectableChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
		{
			NavItem item = dependencyObject as NavItem;
			bool selectable = (bool)args.NewValue;

			item.hoverBorder.Visibility = selectable ? Visibility.Visible : Visibility.Hidden;
		}
	}
}
