﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Controls;
using UPFUI.UserControls.NavigationMenu;
using UPFUI_Test.Pages;

namespace UPFUI_Test.ViewModels
{
	class MainWindowVM : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		readonly Page Page1 = new TestPage1();
		readonly Page Page2 = new TestPage2();
		readonly Page Page3 = new TestPage3();

		private IMenuItem selectedItem = null;

		public IMenuItem SelectedItem
		{
			get { return selectedItem; }
			set
			{
				selectedItem = value;
				SwitchPage(selectedItem);
			}
		}

		public Page SelectedPage { get; private set; }

		public string MenuTitle { get; } = "Test Menu";

		public List<IMenuItem> Menu { get; } = new List<IMenuItem>()
		{
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item 1", IconChar="\uE963", Title="Item 1"},
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item 2", IconChar="\uE962", Title="Item 2"},
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item 3", IconChar="\uE77F", Title="Item 3"},
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item 4", IconChar="\uE77F", Title="Item 4"},
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item 5", IconChar="\uE821", Title="Item 5"},
		};

		public List<IMenuItem> Footer { get; } = new List<IMenuItem>()
		{
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item F1", IconChar="\uE713", Title="FItem 1"},
			new UPFUI.UserControls.NavigationMenu.MenuItem { Id="Test item F2", IconChar="\uE713", Title="FItem 2"},
		};

		public MainWindowVM()
		{

			SelectedItem = Menu[0];
		}

		private void SwitchPage(IMenuItem menuItem)
		{
			switch (menuItem.Id)
			{
				case "Test item 1":
					SelectedPage = Page1;
					break;
				case "Test item 2":
					SelectedPage = Page2;
					break;
				case "Test item 3":
					SelectedPage = Page3;
					break;
			}
		}
	}
}
